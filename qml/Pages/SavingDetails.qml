/*
 * Copyright (C) 2022  Comiryu
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * putaside is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.7
import Ubuntu.Components 1.3
//import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import Qt.labs.settings 1.0
import Ubuntu.Components.Popups 1.3
import "../Components"
import "../js/Database.js" as MyDB
import "../js/Helper.js" as Helper

Page {
  id: pageSavings

  property int id: -1
  property string pageTitle: "[Title]"
  property double amount: 0.00
  property double saved: 0.00
  property string notes: "[Notes]"
  property string url: "[URL]"
  property var rectangleObject: null
  property var progressObject: null
  property var amountObject: null

  header: BaseHeader {
    title: pageTitle

    flickable: detailsFlickable

    trailingActionBar {
       actions: [
         Action {
            iconName: "delete"
            text: i18n.tr("Delete")

            onTriggered: PopupUtils.open(deleteEntryDialogComponent)
         },
         Action {
           id: saveAction

           visible: false

           iconName: "ok"
           text: i18n.tr("Save")

           onTriggered: {
             try {
               MyDB.updateSaving(id, amountTextField.text, urlTextArea.text, notesTextArea.text);
             } catch (err) {
               console.log("Could not update entry: " + err);
             } finally {
               amountLabel.text = amountTextField.text
               urlLabel.text = urlTextArea.text
               notesLabel.text = notesTextArea.text

               amountObject.text = amountTextField.text
               progressObject.value = Helper.getSavingProgress(id, amountTextField.text);
             }

             this.visible = false
             editAction.visible = true
           }
         },
         Action {
           id: editAction

           visible: true

           iconName: "edit"
           text: i18n.tr("Edit")

           onTriggered: {
             this.visible = false
             saveAction.visible = true
           }
         }
      ]
    }
  }

  ScrollView {
        width: parent.width
        height: parent.height
        contentItem: detailsFlickable
    }


  Flickable {
        id: detailsFlickable
        width: parent.width
        height: parent.height
        contentHeight: detailsColumn.height + units.gu(8)
        topMargin: units.gu(2)

        Column {
            id: detailsColumn
            width: parent.width
            anchors.top: parent.top
            anchors.topMargin: units.gu(1)
            spacing: units.gu(2)

            Rectangle {
              anchors {
                left: parent.left
                right: parent.right
                leftMargin: settings.margin
                rightMargin: settings.margin
              }

              radius: units.gu(1)
              color: settings.cardColor

              height: changeButton.visible  ? howMuchLabel.height + amountLabel.height + reduceButton.height + changeButton.height + settings.margin * 4.5
                                            : howMuchLabel.height + amountLabel.height + reduceButton.height + settings.margin * 3

              Label {
                id: howMuchLabel

                anchors {
                  left: parent.left
                  top: parent.top
                  leftMargin: settings.margin
                  topMargin: settings.margin
                }
                width: parent.width / 2

                text: i18n.tr('Price')
                textSize: Label.Small
              }

              Label {
                id: amountLabel

                visible: !amountTextField.visible

                anchors {
                  left: parent.left
                  top: howMuchLabel.bottom
                  leftMargin: settings.margin
                  topMargin: units.gu(1)
                }

                width: parent.width / 2

                text: amount.toFixed(2)
                textSize: Label.XLarge
              }

              TextField {
                id: amountTextField

                visible: saveAction.visible

                anchors {
                  left: parent.left
                  top: howMuchLabel.bottom
                  leftMargin: settings.margin
                  topMargin: units.gu(1)
                }

                width: parent.width / 2
                inputMethodHints: Qt.ImhFormattedNumbersOnly

                text: amountLabel.text
              }

              Label {
                id: savedLabel

                anchors {
                  left: howMuchLabel.right
                  top: parent.top
                  right: parent.right
                  leftMargin: settings.margin
                  topMargin: settings.margin
                }

                text: i18n.tr('Saved')
                textSize: Label.Small
              }

              Label {
                id: savedAmountLabel

                anchors {
                  left: amountLabel.right
                  top: savedLabel.bottom
                  right: parent.right
                  leftMargin: settings.margin
                  topMargin: units.gu(1)
                }

                text: saved.toFixed(2)
                textSize: Label.XLarge
              }

              Button {
                id: reduceButton

                anchors {
                  left: parent.left
                  top : savedAmountLabel.bottom
                  leftMargin: settings.margin
                  topMargin: settings.margin
                  bottomMargin: settings.margin
                }

                width: (parent.width / 2) - (settings.margin * 1.5)

                color: theme.palette.normal.negative
                text: i18n.tr('Remove')

                onClicked: {
                  reduceTextField.visible = true
                  addTextField.visible = false
                }
              }

              Button {
                id: addButton

                anchors {
                  right: parent.right
                  top: savedAmountLabel.bottom
                  rightMargin: settings.margin
                  topMargin: settings.margin
                  bottomMargin: settings.margin
                }

                width: (parent.width / 2) - (settings.margin * 1.5)

                text: i18n.tr('Add')
                color: theme.palette.normal.positive

                onClicked: {
                  addTextField.visible = true
                  reduceTextField.visible = false
                }
              }

              TextField {
                id: addTextField

                visible: false

                anchors {
                  top: addButton.bottom
                  left: parent.left
                  right: changeButton.left
                  topMargin: settings.margin
                  rightMargin: settings.margin
                  leftMargin: settings.margin
                }
                inputMethodHints: Qt.ImhFormattedNumbersOnly
                placeholderText: i18n.tr('How much did you save?')
              }

              TextField {
                id: reduceTextField

                visible: false

                anchors {
                  top: addButton.bottom
                  left: parent.left
                  right: changeButton.left
                  topMargin: settings.margin
                  rightMargin: settings.margin
                  leftMargin: settings.margin
                }
                inputMethodHints: Qt.ImhFormattedNumbersOnly
                placeholderText: i18n.tr('How much do you want to take out?')
              }

              Button {
                id: changeButton

                visible: addTextField.visible || reduceTextField.visible

                anchors {
                  right: parent.right
                  top: addButton.bottom
                  rightMargin: settings.margin
                  topMargin: settings.margin
                }

                width: units.gu(4)
                color: "transparent"

                Icon {
                    anchors.centerIn: parent
                    width: units.gu(3)
                    height: units.gu(3)
                    name: "ok"
                }

                onClicked: {
                              let date = new Date();
                              let formattedDate = date.toLocaleDateString(Qt.locale(),Locale.ShortFormat) + " " + date.toLocaleTimeString(Qt.locale(),Locale.ShortFormat)
                              if(addTextField.text != null && addTextField.text > 0) {
                                MyDB.writeSavingHistory(id, addTextField.text)
                                saved = (saved + addTextField.text * 1).toFixed(2)
                              } else if(reduceTextField.text != null && reduceTextField.text > 0 && saved - reduceTextField.text >= 0) {
                                MyDB.writeSavingHistory(id, reduceTextField.text * -1)
                                saved = (saved - reduceTextField.text * 1).toFixed(2)
                              }

                              progressObject.value = Helper.getSavingProgress(id, amount);

                              historyListModel.clear()
                              Helper.getHistory(id)

                              addTextField.visible = false
                              reduceTextField.visible = false
                            }
              }
            }

            Rectangle {
              anchors {
                left: parent.left
                right: parent.right
                leftMargin: settings.margin
                rightMargin: settings.margin
              }

              radius: units.gu(1)
              color: settings.cardColor
              height: saveAction.visible ? urlHeaderLabel.contentHeight + urlTextArea.height + (settings.margin * 3) : urlHeaderLabel.contentHeight + urlLabel.contentHeight + (settings.margin * 3)

              Label {
                id: urlHeaderLabel

                anchors {
                  left: parent.left
                  top: parent.top
                  right: parent.right
                  leftMargin: settings.margin
                  topMargin: settings.margin
                  rightMargin: settings.margin
                }

                text: i18n.tr('Link')
                textSize: Label.Small
              }

              Label {
                id: urlLabel

                visible: !saveAction.visible

                anchors {
                  left: parent.left
                  right: openUrlButton.left
                  top: urlHeaderLabel.bottom
                  topMargin: settings.margin
                  leftMargin: settings.margin
                  rightMargin: settings.margin
                }

                wrapMode: Text.Wrap
                text: url
              }

              TextArea {
                id: urlTextArea

                visible: saveAction.visible

                anchors {
                  left: parent.left
                  right: openUrlButton.left
                  top: urlHeaderLabel.bottom
                  topMargin: settings.margin
                  leftMargin: settings.margin
                  rightMargin: settings.margin
                }

                text: urlLabel.text
              }

              Button {
                id: openUrlButton

                anchors {
                  right: parent.right
                  top: parent.top
                  bottom: parent.bottom
                  topMargin: settings.margin
                  rightMargin: settings.margin
                  bottomMargin: settings.margin
                }

                width: units.gu(4)

                color: "transparent"

                Icon {
                    anchors.centerIn: parent
                    width: units.gu(3)
                    height: units.gu(3)
                    name: "external-link"
                }

                onClicked: Qt.openUrlExternally(urlLabel.text);
              }
            }

            Rectangle {
              anchors {
                left: parent.left
                right: parent.right
                leftMargin: settings.margin
                rightMargin: settings.margin
              }

              radius: units.gu(1)
              color: settings.cardColor
              height: saveAction.visible ? notesHeaderLabel.contentHeight + notesTextArea.height + (settings.margin * 3) : notesHeaderLabel.contentHeight + notesLabel.contentHeight + (settings.margin * 3)

              Label {
                id: notesHeaderLabel

                anchors {
                  left: parent.left
                  top: parent.top
                  right: parent.right
                  leftMargin: settings.margin
                  topMargin: settings.margin
                  rightMargin: settings.margin
                }

                text: i18n.tr('Notes')
                textSize: Label.Small
              }

              Label {
                id: notesLabel

                visible: !saveAction.visible

                anchors {
                  left: parent.left
                  right: parent.right
                  top: notesHeaderLabel.bottom
                  topMargin: settings.margin
                  leftMargin: settings.margin
                  rightMargin: settings.margin
                }

                wrapMode: Text.Wrap
                text: notes
              }

              TextArea {
                id: notesTextArea

                visible: saveAction.visible

                anchors {
                  left: parent.left
                  right: parent.right
                  top: notesHeaderLabel.bottom
                  topMargin: settings.margin
                  leftMargin: settings.margin
                  rightMargin: settings.margin
                }

                text: notesLabel.text
              }
            }

            Label {
              id: historyLabel

              anchors {
                left: parent.left
                right: parent.right
                leftMargin: settings.margin * 2
                rightMargin: settings.margin * 2
              }

              text: i18n.tr('History')
              textSize: Label.Large
            }

            ListView {
                id: historyListView

                anchors {
                  left: parent.left
                  right: parent.right
                  leftMargin: settings.margin
                  rightMargin: settings.margin
                }

                height: historyListModel.count * units.gu(6)

                interactive: false

                model: historyListModel
                delegate: historyDelegate
                focus: true

                Component.onCompleted: saved = Helper.getHistory(id);
            }
        }

        Component.onCompleted: {
          let savingData = MyDB.readSaving(id);
          pageTitle = savingData.rows.item(0).title
          amount = savingData.rows.item(0).amount
          notes = savingData.rows.item(0).notes
          url = savingData.rows.item(0).url
        }
    }

    Component {
        id: historyDelegate

        ListItem {
            id: historyItemDelegate

            ListItemLayout {
                anchors.centerIn: parent
                title.text: value
                subtitle.text: date
            }
        }
    }

    ListModel {
        id: historyListModel
        property string date: ""
    }

    Component {
        id: deleteEntryDialogComponent
        Dialog {
          id: deleteEntryDialog
          title: i18n.tr("Delete entry")
          text: i18n.tr("Are you sure you want to delete this entry?")

          Button {
              text: i18n.tr("Delete")
              color: theme.palette.normal.negative

              onClicked: {
                PopupUtils.close(deleteEntryDialog)
                MyDB.deleteSaving(id);
                rectangleObject.destroy()
                pageStack.pop();
              }
          }

          Button {
              text: i18n.tr("Cancel")

              onClicked: {
                PopupUtils.close(deleteEntryDialog)
              }
          }
        }
      }
}
